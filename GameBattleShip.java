package sks.sergey;
/**
 * Created by SKS on 25.08.2017.
 */
/*
«Морской бой» — игра для двух участников, в которой игроки по очереди называют координаты на неизвестной им карте
соперника. Если у соперника по этим координатам имеется корабль (координаты заняты), то корабль или его часть
 «топится», а попавший получает право сделать ещё один ход. Цель игрока — первым поразить все корабли противника.

Описание
Требуется реализовать сетевой сервер для игры "Морской бой" с возможностью игры PvP/PvE (с людьми и ботами).

Требования
+	Поддержка поля разных размеров
+	Поддержка возможности играть по сети
+	Поддержка разных типов кораблей
+	Поддержка более чем одной игры на сервере
+	Возможность играть с ботами. Игры человек / человек, человек / бот, бот / бот.
+	Возможность вести запись ходов
+	Реализация проверки корректности ходов:
-	Нельзя ходить не в свой ход
-	Нельзя ходить дважды
-	Нельзя дважды ходить в одно и то же поле
 */


import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

class GameBattleShip extends JFrame {
                                                                        // БЛОК КОНСТАНТ
    final String TITLE = "Морской бой.";                                // титульник
    final String NEW_GAME = "Начало новой игры!!!";
    final String BTN_NEW_GAME = "Новая игра";
    final String BTN_EXIT = "Выход";
    final String YOU_WON = " Вы победили!";
    final String YOU_LOST = " Вы проиграли!";
    final String I_WOUND = " Я попал!";
    final String I_NOT_TO_GET = " Я мимо!";
    final String HE_NOT_TO_GET = " Он мимо!";
    final String HE_WOUNDED = " Он попал";
    final int SIZE_FIELD = 10;                                          // кол-во клеток, 10х10 клеток
    final int PLAYING_FIELD = 600;                                      // размер игрового поля
    final int CELL_SIZE = PLAYING_FIELD / SIZE_FIELD;                   // размер одной ячейки
    final int MY_PANEL_SIZE = PLAYING_FIELD / 2;                        // размер моего игрового поля
    final int MY_CELL_SIZE = MY_PANEL_SIZE / SIZE_FIELD;                // размер моей ячейки
    final int MOUSE_BUTTON_LEFT = 1;                                    // слушатель мышь лев кнопка
    final int MOUSE_BUTTON_RIGHT = 3;                                   // слушатель мышь правая кнопка

    JTextArea board;                                                    // вход
    Canvas rightPanel, myPanel;                                         // игровые поля
    Ships opponentShips, myShips;                                       // коробли врага, мои корабли
    Shots myShots, opponentShots;                                       // выстрелы мои коробли, оппонента
    Random random;
    boolean gameOver;

    public static void main(String[] args) {
        new GameBattleShip();
    }

    GameBattleShip() {
        setTitle(TITLE);                                                // титульник
        setDefaultCloseOperation(EXIT_ON_CLOSE);                        // на крестик - закрыть программу
        setResizable(false);                                            // окно не раздвижное

        JButton init = new JButton(BTN_NEW_GAME);                       // кнопка Новая игра
        init.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                init();
                rightPanel.repaint();                                   // перерисовываем поле боя (оппонента)
                myPanel.repaint();                                      // перерисовываем поле с моими короблями
            }
        });

        JButton exit = new JButton(BTN_EXIT);                           // кнопка Конец игры
        exit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);                                 // при на житии выходим из системы
            }
        });

        myPanel = new Canvas();                                        // панель с моими короблями
        myPanel.setPreferredSize(new Dimension(MY_PANEL_SIZE, MY_PANEL_SIZE));  // размеры
        myPanel.setBackground(Color.white);                                     // фон
        myPanel.setBorder(BorderFactory.createLineBorder(Color.blue));          // границы - цвет


        rightPanel = new Canvas();                                      // панель с короблями оппонента
        rightPanel.setPreferredSize(new Dimension(PLAYING_FIELD, PLAYING_FIELD));   // размеры
        rightPanel.setBackground(Color.white);                                      // фон
        rightPanel.setBorder(BorderFactory.createLineBorder(Color.blue));           // границы - цвет

        rightPanel.addMouseListener(new MouseAdapter() {                // слушаем действие мыши
            @Override
            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                int x = e.getX()/ CELL_SIZE;                            // координата ячейки по X
                int y = e.getY()/ CELL_SIZE;                            // координата ячейки по Y

                if (e.getButton() == MOUSE_BUTTON_LEFT && !gameOver)    // лев кнопкой мыши и не конец игры
                    if (!myShots.hitSamePlace(x, y)) {                  // если попал
                        myShots.add(x, y, true);
                        if (opponentShips.checkHit(x, y)) {             // поражние цели
                            board.append("\n" + (x + 1) + ":" + (y + 1) + I_WOUND);
                            if (!opponentShips.checkSurvivors()) {
                                board.append("\n" + YOU_WON);           // Вы победили
                                gameOver = true;                        // конец игры
                            }
                        } else{
                            board.append("\n" + (x + 1) + ":" + (y + 1) + I_NOT_TO_GET);
                            shootsBot();                              // если не попал - ходит оппонент
                        }
                        rightPanel.repaint();
                        myPanel.repaint();
                        board.setCaretPosition(board.getText().length());
                    }

                    if (e.getButton() == MOUSE_BUTTON_RIGHT) {          // прав кнопкой мыши - ставим маркер
                    Shot label = myShots.getLabel(x, y);
                    if (label != null)
                        myShots.removeLabel(label);
                    else
                        myShots.add(x, y, false);
                    rightPanel.repaint();
                }
            }
        });

        board = new JTextArea();                                                    // табло с ходами
        board.setEditable(false);
        JScrollPane scroll = new JScrollPane(board);

        JPanel buttonPanel = new JPanel();                                          // панель кнопок
        buttonPanel.setLayout(new GridLayout());
        buttonPanel.add(init);
        buttonPanel.add(exit);

        JPanel leftPanel = new JPanel();                                           // левая панель
        leftPanel.setLayout(new BorderLayout());
        leftPanel.add(buttonPanel, BorderLayout.NORTH);                             // + панель кнопок - сверху
        leftPanel.add(scroll, BorderLayout.CENTER);                                 // + панель табло с ходами - центр
        leftPanel.add(myPanel, BorderLayout.SOUTH);                                 // + панель с моими короблями - низ

        setLayout(new BoxLayout(getContentPane(), BoxLayout.X_AXIS));               // рисуем все
        add(leftPanel);                                                             // + левая панель
        add(rightPanel);                                                            // + правая панель
        pack();
        setLocationRelativeTo(null);                                                // в центр
        setVisible(true);                                                           // показать все
        init();
    }

    void init() {                                                                     // init + все объекты игры
        opponentShips = new Ships(SIZE_FIELD, CELL_SIZE, true);                 // + коробли оппонента
        myShips = new Ships(SIZE_FIELD, MY_CELL_SIZE, false);                   // + мои коробли
        opponentShots = new Shots(MY_CELL_SIZE);
        myShots = new Shots(CELL_SIZE);
        board.setText(NEW_GAME);
        gameOver = false;
        random = new Random();
    }

    void shootsBot() {                                                              // алгоритм Бота
        int x, y;
        do {
            x = random.nextInt(SIZE_FIELD);                                         // случайная координата Х
            y = random.nextInt(SIZE_FIELD);                                         // случайная координата Y
        } while (opponentShots.hitSamePlace(x, y));
        opponentShots.add(x, y, true);
        if (!myShips.checkHit(x, y)) {                                              // Бот стреляет Мимо меня
            board.append("\n" + (x + 1) + ":" + (y + 1) + HE_NOT_TO_GET);
            return;
        } else {                                                                    // Бот попадает в меня + еще ход
            board.append("\n" + (x + 1) + ":" + (y + 1) + HE_WOUNDED);
            board.setCaretPosition(board.getText().length());
            if (!myShips.checkSurvivors()) {
                board.append("\n" + YOU_LOST);                                        // Бот выиграл
                gameOver = true;
            } else
                shootsBot();
        }
    }

    class Canvas extends JPanel {                                                   // рисуем
        @Override
        public void paint(Graphics g) {
            super.paint(g);
            int cellSize = (int) getSize().getWidth() / SIZE_FIELD;
            g.setColor(Color.lightGray);
            for (int i = 1; i < SIZE_FIELD; i++) {
                g.drawLine(0, i*cellSize, SIZE_FIELD *cellSize, i*cellSize);    // горизонт линии
                g.drawLine(i*cellSize, 0, i*cellSize, SIZE_FIELD *cellSize);    // вертик линии
            }
            if (cellSize == CELL_SIZE) {
                myShots.paint(g);
                opponentShips.paint(g);
            } else {
                opponentShots.paint(g);
                myShips.paint(g);
            }
        }
    }
}
