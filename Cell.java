package sks.sergey;

/**
 * Created by SKS on 25.08.2017.
 */

import java.awt.Color;
import java.awt.Graphics;

class Cell {
    private final Color BLUE = Color.blue;
    private int x, y;
    private Color color;

    Cell(int x, int y) {
        this.x = x;
        this.y = y;
        color = Color.gray;
    }

    int getX() { return x; }
    int getY() { return y; }

    boolean checkHit(int x, int y) {
        if (this.x == x && this.y == y) {
            color = BLUE;
            return true;
        }
        return false;
    }

    boolean isAlive() {
        return color != BLUE;
    }

    void paint(Graphics g, int cellSize, boolean hide) {
        if (!hide || (hide && color == BLUE)) {
            g.setColor(color);
            g.fill3DRect(x*cellSize + 1, y*cellSize + 1, cellSize - 2, cellSize - 2, true);
        }
    }
}
